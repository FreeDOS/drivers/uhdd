# UHDD

UHDD is a hard disk/SSD caching driver for up to 10 BIOS disks on up to 4 UltraDMA controllers. It will also provide caching to disks handled by UDVD2, if it is loaded after UHDD.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## UHDD.LSM

<table>
<tr><td>title</td><td>UHDD</td></tr>
<tr><td>version</td><td>2021-10-30</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-10-30</td></tr>
<tr><td>description</td><td>Hard disk/SSD caching driver</td></tr>
<tr><td>summary</td><td>UHDD is a hard disk/SSD caching driver for up to 10 BIOS disks on up to 4 UltraDMA controllers. It will also provide caching to disks handled by UDVD2, if it is loaded after UHDD.</td></tr>
<tr><td>keywords</td><td>cache, disk, dma, driver, ide, sata, ultradma</td></tr>
<tr><td>author</td><td>Jack R. Ellis</td></tr>
<tr><td>maintained&nbsp;by</td><td>Mercury Thirteen (mercury0x0d@protonmail.com)</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://www.mercurycoding.com/downloads.html</td></tr>
<tr><td>mirror&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/ellis</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>Free, with source code, no warranty</td></tr>
</table>
